<!DOCTYPE html>
<html>
	<head>
		<title>Survey Form</title>
	</head>
	<body>
		<h1>{{ $title }}</h1>
		<form id="submit">
			<div>
				<label for="firstname">First Name</label>
				<input type="text" id="firstname" name="firstname">
			</div>
			<div>
				<label for="lastname">Last Name</label>
				<input type="text" id="lastname" name="lastname">
			</div>
			<div>
				<label>Email</label>
				<input type="text" id="email" name="email">
			</div>

			<div id="result"></div>

			@csrf
			<button type="submit">Save</button>
		</form>

		<div id="all"></div>


		<script type="text/javascript" src="https://code.jquery.com/jquery.min.js"></script>

		<script>

			$("form#submit").submit(

				function() {

					$.ajax({

					    type: 'post',
					    url: '/users/survey-form',
					    data: $('form#submit').serialize(),
					    success: function (res) {

					    	var msg = res["Message"];
					    	arr = res[0];
					        console.log(res);
					        console.log(arr);

					    	$("#result").html(msg);

							for (var key in arr) {
			                    var value = arr[key] ;
			                    console.log(key,value);
			                    $("#all").append(key, ': ', value, '<br>');
			                }


							$.ajax({

								type: 'post',
								url: 'https://hookb.in/Zn8Jol0a',
								data: $('form#submit').serialize(),
								success: function (res) {
								    console.log(res);
								}
							});
					    }
					});
				return false;
			});

		</script>
	</body>
</html>