<!DOCTYPE html>
<html>
	<head>
		<title>Login Page</title>
		<meta name="referrer" content="no-referrer-when-downgrade" />
	</head>
	<body>
		<h1>Login Page</h1>
		<form id="submit">
			<div>
				<label for="username">Username</label>
				<input type="text" id="username" name="username">
			</div>
			<div>
				<label for="password">Password</label>
				<input type="password" id="password" name="password">
			</div>

			<div id="error"></div>

			@csrf
			<button type="submit">Login</button>
		</form>

		<script type="text/javascript" src="https://code.jquery.com/jquery.min.js"></script>

		<script>

			$("form#submit").submit(

				function() {

					$.ajax({

					    type: 'post',
					    url: '/login-page',
					    data: $('form#submit').serialize(),
					    success: function (res) {
					        if (res == '1'){
					        	window.location.href = 'users/survey-form';
					        }
					        else
					        {
					        	alert("Invalid username or password!");
					        	window.location.href = 'login-page';
					        }
					    }
					});
					return false;
				});

		</script>
	</body>
</html>