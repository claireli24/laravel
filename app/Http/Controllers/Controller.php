<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Auth\AuthenticationException;
use Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public static function submit_survey_form(){

    	$firstname="YS";
		$lastname=$_POST['lastname'];
		$email=$_POST['email'];
		$response = "submit successfully";

		$alldata = array("Message" => $response, ["First Name" => $firstname, "Last Name" => $lastname,  "Email" => $email]);
		return response()->json($alldata);
    }

    public function abc(){
    	return view('layouts/survey-form')->with('title','ABC form');
    }


    public function return_login(){

    	return view('layouts/login-page');
    }

    public function check_credentials(){

    	$username=$_POST['username'];
		$password=$_POST['password'];
		$response = true;

		if ($username == "claire@easystore.co" && $password == "123456")
        {
        	$cookie_name = "logged";
			$cookie_value = 1;
			setcookie($cookie_name, $cookie_value, time() + (86400 * 30), "/");
			
        	return response()->json($cookie_value);
        }
	}
}