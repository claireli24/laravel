<?php


namespace App\Http\Middleware;


use Closure;


class CheckLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (isset($_COOKIE["logged"]) && $_COOKIE['logged'] == 1) {
            return $next($request);
        }
        else
        {
            return redirect()->action('Controller@return_login');
        }
    }
}