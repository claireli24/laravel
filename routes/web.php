<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('users/survey-form', 'Controller@abc')->middleware('check_login');

Route::post('users/survey-form' , 'Controller@submit_survey_form');


Route::get('login-page' , 'Controller@return_login')->name('login');

Route::post('login-page' , 'Controller@check_credentials');

Route::any('{all}', function(){
    return redirect()->action('Controller@return_login');
})->where('all', '.*');